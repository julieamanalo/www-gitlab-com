---
layout: markdown_page
title: Service Engineer Onboarding
---

## Mentions Channel

We use the [mentions-of-gitlab](https://gitlab.slack.com/messages/mentions-of-gitlab/) slack channel to track mentions of GitLab across multiple sources. This allows us to respond to user requests across various platforms.

We currently track the following sources for GitLab mentions:

1. Product Hunt
2. Tumblr
3. HackerNews
4. Reddit

We use [notify.ly](https://notify.ly/)  to track and pipe these mentions to the mentions-of-gitlab channel.

We also funnel all comments on our [blog posts](https://about.gitlab.com/blog-posts) and any mention of GitLab on [Lobsters](https://lobste.rs/) to this channel using [zappier](https://zapier.com/).